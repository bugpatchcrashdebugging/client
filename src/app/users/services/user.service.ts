import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {User} from '../model/user';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
  private url = 'http://localhost:5050/api/v1';

  constructor(private http: HttpClient) { }
  // create
  sendNewUser(user: User) {
    return this.http.post(this.url + '/', user);
  }
  // get by id
  getUser(id: string): Observable<User> {
    return this.http.get<User>(`${this.url}/${id}`);
  }
  // get all
  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(this.url + '/'); // can remove last '/' ?
  }
  // delete
  deleteUser(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
  // Update
  updateUser(id: string, user: User): Observable<User> {
    return this.http.put<User>(`${this.url}/${id}`, user);
  }
}
