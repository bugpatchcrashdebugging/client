import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'; // Why I should import this twice
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-manipulate-user',
  templateUrl: './manipulate-user.component.html',
  providers: [UserService]
})
export class ManipulateUserComponent implements OnInit {
  private userForm: FormGroup;
  private pageTitle = 'Add User';
  private id;

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router?: Router,
    private route?: ActivatedRoute) {
  }

  ngOnInit() {
      this.id = this.getCurrentId();
      this.userForm = this.fb.group({
      'firstName': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'lastName': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'address': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'phone': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ])
    });

    if ('undefined' !== typeof this.id) {
      this.pageTitle = 'Update user';
      this.service.getUser(this.id).subscribe( user => this.userForm.patchValue(user));
    }
  }

  onSubmit() {

    const subscr = () => this.router.navigate(['/users']);

    const formUser = {
      firstName: this.userForm.controls['firstName'].value,
      lastName: this.userForm.controls['lastName'].value,
      address: this.userForm.controls['address'].value,
      phone: this.userForm.controls['phone'].value
    };

    if ('undefined' !== typeof this.id) {
      this.service.updateUser(this.id, formUser).subscribe(subscr);
    } else {
      this.service.sendNewUser(formUser).subscribe(subscr);
    }
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.userForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }
  getCurrentId(): string {
    let id;
    this.route.params.subscribe( params => id = params['id']);
    return id;
  }
}
