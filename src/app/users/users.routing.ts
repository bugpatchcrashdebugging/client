import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { UsersComponent } from './users.component';
import { ManipulateUserComponent } from './components/manipulate-user.component';

export  const routes: Routes = [
    { path: 'users/:id/update', component: ManipulateUserComponent },
    { path: 'users', component: UsersComponent },
    { path: 'users/create_user', component: ManipulateUserComponent },
  ];

  export  const routing: ModuleWithProviders = RouterModule.forChild(routes);
