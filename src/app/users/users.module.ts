import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { UsersComponent } from './users.component';
import { UserService } from './services/user.service';
import { routing } from './users.routing';
import { ManipulateUserComponent } from './components/manipulate-user.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing
    ],
    declarations: [UsersComponent, ManipulateUserComponent ],
    providers: [UserService],
    exports: [UsersComponent]
})
export class UserModule { }
