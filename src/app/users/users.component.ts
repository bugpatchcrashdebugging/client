import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { User } from './model/user';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  private users: User[];
  constructor(
    private service: UserService) {
  }

  ngOnInit() {
    this.service.getAllUser().subscribe( users => {
      this.users = users;
    });
  }

  onSubmitDel(id: string) {
    const deleteReq = this.service.deleteUser(id);

    return deleteReq.pipe(
    switchMap(() => this.service.getAllUser())).subscribe(
      users => {
      this.users = <User[]>users;
    });
  }
}
