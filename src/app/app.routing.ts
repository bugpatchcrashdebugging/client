import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';

export  const routes: Routes = [
    {
        path: 'page',
        component: AppComponent,
        children: [
            { path: '', component: UsersComponent }
        ]
    }
  ];

export  const routing: ModuleWithProviders = RouterModule.forRoot(routes);
